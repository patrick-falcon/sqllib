package com.falcon.sqllib.config;

/**
 *
 * @author patrick
 */
public abstract class ConfigAPI {
    
    private static String host,username,password,database;
    
    public static void init() {
        host="null";
        username="null";
        password="null";
        database="null";
    }

    public static String getHost() {
        return host;
    }

    public static String getUsername() {
        return username;
    }

    public static String getPassword() {
        return password;
    }

    public static String getDatabase() {
        return database;
    }

    public static void setDatabase(String database) {
        ConfigAPI.database = database;
    }

    public static void setHost(String host) {
        ConfigAPI.host = host;
    }

    public static void setPassword(String password) {
        ConfigAPI.password = password;
    }

    public static void setUsername(String username) {
        ConfigAPI.username = username;
    }
    
    
    
    
    
}
