package com.falcon.sqllib;

import com.falcon.sqllib.config.ConfigAPI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Patrick Falcon
 */
public class SQLLib {
    
    private Connection connection;
    
    public SQLLib() {
        ConfigAPI.init();
    }
    
    public void establishConnection() throws SQLException {
        String port = "3306";
        
        if (connection != null || !connection.isClosed()) {
            connection.close();
            System.out.println("Lingering connection closed");
            System.out.println("Connection needs to be established again");
            return;
        }
        
        connection = DriverManager.getConnection("jdbc:mysql://"+ConfigAPI.getHost() + ":" + port + "/" + ConfigAPI.getDatabase(),
                ConfigAPI.getUsername(),ConfigAPI.getPassword());
    }
    
    public void closeConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            System.out.println("Error whilst closing connection");
            return;
        }
        
        connection.close();
        System.out.println("Connection closed.");
    }
    
    public int displayInt(String table, String key, String identifier) throws SQLException {
        
        PreparedStatement get = getConnection().prepareStatement("SELECT * FROM " + table + " WHERE "+key+"=?");
        get.setString(1, identifier);
        
        ResultSet rs = get.executeQuery();
        
        if (rs.next()) {
            return rs.getInt(identifier);
        } else {
            System.out.println("Could not load from database.");
        }
        
        return 0;
    }
    
    public Connection getConnection() {
        return connection;
    }
    
    public void setUsername(String s) {
        ConfigAPI.setUsername(s);
    }
    
    public void setHost(String s) {
        ConfigAPI.setHost(s);
    }
    
    public void setPassword(String s) {
        ConfigAPI.setPassword(s);
    }
        
    public void setDatabase(String s) {
        ConfigAPI.setDatabase(s);
    }
    
}
